use <calf.scad>

module calf_left(){
  intersection(){
    calf( false );
    translate ([48/2  + 50, 0,0]){
      cube( size=[100,200,200], center = true );
    }
  }
}


module calf_right(){
  intersection(){
    calf( false );
    translate ([-48/2  - 50, 0,0]){
      cube( size=[100,200,200], center = true );
    }
  }
}


module calf_center(){
  intersection(){
    calf( false );
      cube( size=[48-0.5,200,200], center = true );
  }
}

rotate(90,[0,1,0]){
 //calf_left();
}

rotate(-90,[0,1,0]){
 calf_right();
}

rotate(90,[1,0,0]){
  //calf_center();
}
