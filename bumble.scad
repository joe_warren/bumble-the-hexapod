use <body.scad>
use <head.scad>
use <leg.scad>
use <pi_case.scad>
leg_positions = [
  [
    [40, 60, 40, 60, 40],
    [ 45, 45, 0, 0, 45], 
    [ 45, 45, 0, 0, 45]
  ],
  [
    [20, -20, 20, -20, 20],
    [ 0, 0, 45, 45, 0], 
    [ 0, 0, 45, 45, 0]
  ], 
  [
    [-60, -40, -60, -40, -60],
    [ 45, 45, 0, 0, 45], 
    [ 45, 45, 0, 0, 45]
  ],
  [
    [60, 40, 60, 40, 60],
    [ 0, 0, 45, 45, 0], 
    [ 0, 0, 45, 45, 0]
  ],
  [
    [20, -20, 20, -20, 20],
    [ 45, 45, 0, 0, 45], 
    [ 45, 45, 0, 0, 45]
  ],
  [
    [ -40, -60, -40, -60, -40],
    [ 0, 0, 45, 45, 0], 
    [ 0, 0, 45, 45, 0]
  ] 
];

module all_legs() {
  dx = 90;
  dy = 25; 
  tol = 0.4;
  translate([0, 0, 2]){
    for( r = [0, 180]){
      rotate(a=r, v=[0,0,1]){
        for( x = [-dx, 0, dx]){ 
          translate([x, dy, 0]) {
            leg(leg_positions[(r/180)*3 + 1+x/dx],$t);
          }
        }
      }
    }
  }
}

body();
all_legs();
translate([110 -7.5, 0, 12]){
  translate([32,0,30]){
    head();
  }
}
translate( [45, 0, 10]){
  rotate( 90, [0,0,1]){
    pi_case();
  }
}
