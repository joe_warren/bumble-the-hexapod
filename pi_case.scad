use <rounded_corner.scad>

module pi_case(){
  difference(){
    union(){
      translate( [0,0,30/2 + 2] ){
        rounded_cube( [92, 60, 30], 3);
      }
      for( i = [-1, 1] ){
        for( j = [-1, 1] ){
           translate( [i*(92/2 - 5), j*(60/2 -5), 1 ]){
             cylinder( r = 2.5, h = 2, center = true, $fn=12 );
           }
         }
      }
    }
    translate( [0,0,30/2 + 2] ){
      cube( size=[92-4, 60-4, 30-4], center=true);
    }
    translate( [0,-10,30 + 2] ){
      cube( size=[5, 25, 5], center=true);
    }
    translate( [40,0,30 + 2] ){
      cube( size=[5, 25, 5], center=true);
    }
    translate( [10,30,30 + 2] ){
      cube( size=[60, 20, 10], center=true);
    }
    for( i = [-1, 0, 1] ){
      translate( [-92/2, 17*i, 30/2+4 ]){
        cube( size=[10,12,16],center = true );
      }
    }
    for( i = [-1, 1] ){
      translate( [i*47.5/2, 0, 2+2 ]){
        cylinder( r = 2.5, h = 5, center = true, $fn=12 );
      }
    }
  }
}
pi_case();
