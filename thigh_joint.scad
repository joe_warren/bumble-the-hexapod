use <servo_mount.scad>

module thigh_joint(include_servo = true){
  tol = 1;
  union(){
    servo_mount(include_servo);
    rotate(180, [0,0,1]){
      translate([0, -80, 0]){
        servo_mount(include_servo);
      }
    }
    translate([0, 40, -8-36 -5/2 -tol/4]){
      cube (size=[21.5, 62, 5-tol/2], center=true );
    }
  }
}

thigh_joint(false);
