module rounded_corner(){
  difference(){
    cube( size =[40, 40, 20], center = true);
    translate([-20, -20, 0 ]){
      cylinder( r= 20, h = 22, center = true );
    }
  }
}

module semi_rounded_cube( v, r ){
  difference(){
    cube( size=v, center=true);
    for(i = [1,-1]){
      for( j = [1,-1]){
        translate([v[0]*i/2,v[1]*j/2,0]){
          rotate( 45 * (j-1) + 45 * (i-1) + 45*(i-1)*(j+1), [0,0,1]){ 
            scale( [r/20, r/20, (v[2]+1)/20 ] ){
              rounded_corner();
            }
          }
        }
      }
    }
  }
}

module rounded_cube( v, r ){
  difference(){
    cube( size=v, center=true);
    for(i = [1,-1]){
      for( j = [1,-1]){
        translate([v[0]*i/2,v[1]*j/2,0]){
          rotate( 45 * (j-1) + 45 * (i-1) + 45*(i-1)*(j+1), [0,0,1]){ 
            scale( [r/20, r/20, (v[2]+1)/20 ] ){
              rounded_corner();
            }
          }
        }
      }
    }
    rotate( 90, [1,0,0] ){
      for(i = [1,-1]){
        for( j = [1,-1]){
          translate([v[0]*i/2,v[2]*j/2,0]){
            rotate( 45 * (j-1) + 45 * (i-1) + 45*(i-1)*(j+1), [0,0,1]){ 
              scale( [r/20, r/20, (v[1]+1)/20 ] ){
                rounded_corner();
              }
            }
          }
        }
      }
    }
    rotate( 90, [0,1,0] ){
      for(i = [1,-1]){
        for( j = [1,-1]){
          translate([v[2]*i/2,v[1]*j/2,0]){
            rotate( 45 * (j-1) + 45 * (i-1) + 45*(i-1)*(j+1), [0,0,1]){ 
              scale( [r/20, r/20, (v[0]+1)/20 ] ){
                rounded_corner();
              }
            }
          }
        }
      }
    }
  }
}

rounded_corner();
