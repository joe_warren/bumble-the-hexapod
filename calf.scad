use <servo_mount.scad>
use <split_joint.scad>

module calf(show_skewer=true){
  xDcylinder = 48/2 + 10/2;
  difference(){
    union(){
      for( xt = [-xDcylinder, xDcylinder]){
        translate([xt, 0,0]){
          rotate(90, [0,1,0]){
            cylinder(r=20, h = 10, center = true);
          }
        }
      }
      for( xt = [-xDcylinder, xDcylinder]){
        translate([xt, 20,0]){
          cube( size=[10, 20, 20], center=true);
        }
      }
      translate([0,32,0]){
        cube(size=[48+20, 10, 20], center=true);
      }
      translate([0,42,0]){
        rotate(-90, [1,0,0]){
          difference(){
            cylinder(r=7.5, h=10, center=true);
            cube( size = [6,6,12], center=true );
          }
          if( show_skewer ){
            translate([0,0,30]){
              cube( size=[5,5,60], center= true);
            }
          }
        }
      }
    }

    translate([24,32,0]){
      rotate( 180, [0,0,1] ){
        split_joint();
      }
    }
    translate([-24,32,0]){
      split_joint();
    }
    rotate( 90, [0,1,0]){
      translate( [0,0, 53/2]){
        servo_mount_holes();
      }
    }
  }
}

calf(false);
