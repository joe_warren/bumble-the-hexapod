use <servo_mount.scad>
use <split_joint.scad>

module hip_joint(draw_servos = true){
  xDcube = 15 + (48+20-30)/4;
  xDcylinder = 48/2 + 10/2;
  difference(){
    union(){
      servo_mount( draw_servos );
      translate([0,30+5,- 8 - 10 - 7.5]){
        for( xt = [-xDcube, xDcube]){
          translate([xt, 0,0]){
            cube(size=[(48+20-30)/2,10, 15], center = true);
          }
        }

        for( xt = [-xDcylinder, xDcylinder]){
          translate([xt, 10,0]){
            cube( size=[10, 20, 15], center=true);
          }
        }
        for( xt = [-xDcylinder, xDcylinder]){
          translate([xt, 32,0]){
            rotate(90, [0,1,0]){
              cylinder(r=20, h = 10, center = true);
            }
          }
        }
      }
    }

    translate([-24,30+5,-8 - 10 - 7.5]){
      split_joint();
    }
    translate([24,30+5,-8 - 10 - 7.5]){
      rotate(180, [0,0,1]){
        split_joint();
      }
    }
    translate([0,42+5 + 20,- 8 - 10 - 7.5]){
      rotate( 90, [0,1,0]){
        translate( [0,0, 53/2]){
          servo_mount_holes();
        }
      }
    }

  }
}
//translate( [-30,0,0]) 
//split_part();
hip_joint( false );
