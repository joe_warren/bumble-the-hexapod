use <hk_servo.scad>


module servo_mount_split(){
  tol = 0.5;
  union(){
    for( j = [0,1] ){
     rotate( 180*j, [0,0,1]){
        translate([11,0,-26-5-8-10]){

          translate([6.5/2, 0,10]){
            intersection(){             
              cube( size=[12, 7.5, 5], center=true);
              translate([0, 0,2.5]){
                sphere( r=5, center=true);
              }
            }
          }
          translate([6.5/2, 0,0]){
            cylinder( r=3, h = 3, center=true, $fn =10);
            cylinder( r=1.5, h = 16, center=true, $fn =10);
          }

          translate([6.5+tol/2, 0,0]){
            cube( size=[tol, 15, 10], center=true);
          }
          for( i =[-1,1] ){
            translate([(6.5+tol/2)/2,(15/2 -3/2)*i, 0]){
              rotate( i*atan( 3/6.5),[0,0,1]){
                cube( size=[8, tol, 10], center=true);
              }
            }
  
            translate([0, i*35, 0]){
              cube( size =[tol,2*(35-15/3.5), 10], center=true);
            }
          }
          difference(){
            translate([6.5/2,0,5-tol/2]){
              cube( size =[7.5,15, tol], center=true);
            }
            for( i =[-1,1] ){
              translate([(6.5+tol/2)/2,(15/2 -3/2 + 12*(3/6.5))*i, 0]){
                rotate( i*atan( 3/6.5),[0,0,1]){
                  cube( size=[20, 10, 20], center=true);
                }
              }
            }
          }
        }
      }
    }
  }
}


module servo_mount( include_servo = true ){
  tol = 2;
  translate([0, 10, -8-10]){
    difference(){
      union(){
        translate ([0, 0, -7.5]){
          cube(size=[30,60,15], center=true);
        }
        translate([0,-10,-13-2.5]){
          cube (size=[35, 20, 26+5], center=true );
        }
        translate([0,-10,-26-5-2.5 ]){
          cylinder(r=5,h=5.01,center=true);
        }
      }
      translate ([0,0,-12]){
       cube (size =[20 + tol,40 + tol,27 + tol], center =true) ;
      }
      translate ([0,-20 -3/2,-7.5]){
       cube (size =[7 ,3,20], center =true) ;
      }
      translate ([0,-20 -3/2,-5/2]){
       cube (size =[10 ,3,3.5], center =true) ;
      }
      for(xt = [5, -5]){
        for(yt = [-20-4, 20+4]){
          translate([xt, yt, -1]){
            union(){
              cylinder( r= 4, h = 2.2, center=true, $fn = 10);
              translate([0, 0, -1.1]){
                scale( [1,1,0.75] ){
                  sphere( r =4, center=true );
                }
              }
            }
          }
          translate([xt, yt, -15]){
              cylinder( r= 3.5, h = 4, center=true, $fn = 6);
          }
          translate([xt, yt, -7.5]){
            cylinder( r= 3/2, h = 16, center=true, $fn = 10);
          } 
        }
      }
      translate([0, -10, 8+10]){
        servo_mount_split();
      }
    }
  }
  if( include_servo ){
    hk_servo();
  }
}

module servo_mount_holes( tolerance = 1 ){
  union(){
    hk_servo_hex_top(tolerance);
    translate([0,0,10]){
      // Hole to allow access to screw connecting horn to servo
      cylinder(r=5,h=21,center=true);
    }
    translate([0, 10, -8-10]){
      translate([0,-10,-26-5-2.5 ]){
        cylinder(r=5 + tolerance,h=5+ tolerance ,center=true);
      }
    }
  }
}

servo_mount(false);

//        servo_mount_split();
