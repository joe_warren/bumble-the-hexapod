module pi_camera_cube(){
  tol = 2;
  cube( size =[25+ tol, 30,24+tol], center = true );
  translate( [0,0,-12-tol/2]){
    cube( size =[20+ tol, 30,12], center = true );
  }
  translate( [0,30,(24+tol)/2.0-5]){
    cube( size =[25+ tol, 32,10], center = true );
  }
}

module guide_holes(){
  translate([0,4,15-5]){
    for( i = [1,-1] ){
      for( j = [1,-1] ){
        translate([ 20 *i, 12*j, 0]){
          cylinder( r = 1.5, h = 20, center = true, $fn=10);
        }
      }
    }
  }
}

module LED_hole(){
  union(){
    cylinder(r=2.5, h=20, center=true, $fn=16);
    translate([0,0,20/2+(4.5/2)-0.001]){
      cylinder(r=2, h=4.5, center=true, $fn=16);
      translate([0,0,4.5/2]){
        sphere(r=2, center=true, $fn=16);
      }
    }
  }
}


module LED_eye_holes(){
  translate([0,4,15-5]){
    for( i = [1,-1] ){
      translate([ 14*i, -12, 0]){
        LED_hole();
      }
    }
  }
}


module pi_camera_holes(){
  rotate(90,[1,0,0]){
    translate([0,3.25, 10]){
      for( i = [1,-1] ){
        for( j = [1,-1] ){
          translate([ 10.5 *i, 6.75*j, 0]){
            cylinder( r = 1.5, h = 30, center = true, $fn=10);
          }
        } 
      }
      translate([ 0, -6.75, 5]){
        cylinder( r = 6, h = 30, center = true, $fn=10);
      }
    }
  }
}

module head_mount(){
  translate([0,25,-30]){
    cylinder( h= 5, r = 25, center=true );
    cube( size =[70,25/2,5], center = true);
    translate([0,70/4,0]){
      cube( size =[15,70/2,5], center = true);
    }
  } 
}

module head_mount_holes(){
  translate([0,25,-30]){
    for( i = [1,-1]){
      translate([i*(35-15/2),0,0]){
        cylinder( r = 3, h = 6, center=true);
      }
    }
    translate([0,(35-15/2),0]){
      cylinder( r = 3, h = 6, center=true);
    }
  } 
}

module head_combined(){
  rotate(90,[0,0,1]){
    union(){
      difference(){
        union(){
          scale(16){
            import("dragon_head.stl"); 
          }
          head_mount();
        }
        translate([0,16,-0.5]){
          pi_camera_cube();
        }
        translate([0,16,-0.5]){
          pi_camera_holes();
        }
        translate([0,16,-0.5]){
          guide_holes();
        }
        translate([0,16,-0.5]){
          LED_eye_holes();
        }
        head_mount_holes();
      }
    }
  }
}
module head_first(){
  intersection(){
    union(){
      translate([-49,0,0]){
        cube( size=[100,1000,200], center = true );
      }
      translate([-49,0,0]){
        cube( size=[100,1000,5], center = true );
      }
    }
    translate([-49,0,-37.51]){
      cube( size=[100,1000,100], center = true );
    }
    head_combined();
  }
}

module head_second(){
  translate([2,0,0]){
    difference(){
      head_combined();
      translate([-49,0,0]){
        cube( size=[100,100,200], center = true );
      }
    } 
  }
}


module head_third(){
  translate([0,0,2]){
    difference(){
      intersection(){
        union(){
          translate([-49,0,0]){
            cube( size=[100,1000,200], center = true );
          }
          translate([-49,0,0]){
            cube( size=[100,1000,5], center = true );
          }
        }
        head_combined();
      }
      translate([-49,0,-37.5]){
        cube( size=[100,1000,100], center = true );
      }
    }
  }
}

module head(){
  head_first();
  head_second();
  head_third();
}

//pi_camera_cube();
//guide_holes();
//LED_eye_holes();
head_combined();
//head();
