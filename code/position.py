#!/usr/bin/env python
import math
leg_lengths = [67, 80, 60]

def squared( x ):
    return x * x

def relativeLegAngles( pos ):
    H = math.sqrt( pos[0]*pos[0] + pos[1]* pos[1] ) - leg_lengths[0]
    V = pos[2]

    ang1 = math.atan2( pos[0], pos[1] )
 
    A = math.sqrt( V * V + H * H )

    numinator = squared(leg_lengths[1]) + squared(leg_lengths[2]) - A * A
    denominator = 2.0 * leg_lengths[1] * leg_lengths[2]
    a = math.acos( numinator/denominator )
    ang3 = a - math.pi

    f = math.atan2( H, V )
    c = math.asin( leg_lengths[2] * math.sin(a) / A )
    ang2 = c + f - math.pi/2.0

    return [ ang1, ang2, ang3]
    
for z in range( 0,100, 10 ):
    a = relativeLegAngles([0, 150, z])
