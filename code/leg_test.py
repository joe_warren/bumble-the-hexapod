#!/usr/bin/env python

from RPIO import PWM

from time import sleep

from math import floor
import math

from position import relativeLegAngles

print "hello servo"

ports = [23, 24, 25] 

w = 1

low = [510, 2440, 510]
high = [2400 ,600 ,2100]
mid = [1300, 1520, 1300]

steps = 10

def roundServoInput( x):
    return 10*floor(x/10)

def servoToAngle(servo, i, a):
    sVal = 2.0 * a / math.pi
    sVal *= (high[i]-low[i])/2
    sVal += (high[i]+low[i])/2
    sVal = roundServoInput(sVal)
    servo.set_servo( ports[i], sVal )

servo = PWM.Servo()
for z in range( 0, 100, 2 ):
    a = relativeLegAngles([0,140,z])
    for i in [0,1,2]:
        servoToAngle( servo, i, a[i] )
    sleep(0.02)

sleep(1)

for x in range( 0, 100, 2 )+ range(100,-100,-2)+ range(-100,0,2):
    print x
    a = relativeLegAngles([x,140,100])
    for i in [0,1,2]:
        servoToAngle( servo, i, a[i] )
    sleep(0.02)

sleep(1)

for z in range( 100, 0, -2 ):
    a = relativeLegAngles([0,140,z])
    for i in [0,1,2]:
        servoToAngle( servo, i, a[i] )
    sleep(0.02)


sleep(5)

for p in ports:
    servo.stop_servo( p )
