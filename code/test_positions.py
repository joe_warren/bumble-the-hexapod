#!/usr/bin/env python
from position import relativeLegAngles
import turtle as t 
import math
from time import sleep

leg_lengths = [80, 80, 60]

#t= turtle

for z in range( 0,100, 10 ):
    a = relativeLegAngles([0, 160, z])
    t.forward( leg_lengths[0] * 2 )
    t.left( a[1] * 180 / math.pi )
    t.forward( leg_lengths[1] * 2 )
    t.left( a[2] * 180 / math.pi )
    t.forward( leg_lengths[2] * 2 )
    t.up()
    t.home()
    t.down()
    sleep(2)

sleep(100)
