use <hip_joint.scad>

module hip_joint_left(){
  intersection(){
    hip_joint( false );
    translate ([48/2  + 50, 0,0]){
      cube( size=[100,200,200], center = true );
    }
  }
}


module hip_joint_right(){
  intersection(){
    hip_joint( false );
    translate ([-48/2  - 50, 0,0]){
      cube( size=[100,200,200], center = true );
    }
  }
}


module hip_joint_center(){
  intersection(){
    hip_joint( false );
      cube( size=[48-0.5,200,200], center = true );
  }
}

rotate(90,[0,1,0]){
 //hip_joint_left();
}

rotate(-90,[0,1,0]){
  hip_joint_right();
}

rotate(180,[1,0,0]){
  //hip_joint_center();
}
