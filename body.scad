use <servo_mount.scad>
use <rounded_corner.scad>

module body_holes() {
  dx = 90;
  dy = 25; 
  tol = 0.4;
  translate([0, 0, 2]){
    for( r = [0, 180]){
      rotate(a=r, v=[0,0,1]){
        for( x = [-dx, 0, dx]){ 
          translate([x, dy, 0]) {
            servo_mount_holes( 1 );
          }
        }
      }
    }
  }
}

module body_top_mesh_holes(){

  translate([110,0,0]){
    cylinder( r= 15, h=30, center=true);
  }
  translate([5,0,0]){
    semi_rounded_cube( [50,15,30], 5 );
  }
  translate([-80,0,0]){
    semi_rounded_cube( [40,15,30], 5 );
  }
  for( i = [-1,1] ){
    translate([-68,i*25,0]){
      semi_rounded_cube( [5,30,30], 2.5 );
    }
    translate([-22,i*25,0]){
      semi_rounded_cube( [5,30,30], 2.5 );
    }
    translate([27,i*25,0]){
      semi_rounded_cube( [16,30,30], 5 );
    }
    translate([63,i*25,0]){
      semi_rounded_cube( [15,30,30], 5 );
    }
  }
}

module body_base_mesh_holes(){
  translate([15,0,0]){
    semi_rounded_cube( [20,30,30], 5 );
  } 
  translate([-15,0,0]){
    semi_rounded_cube( [20,30,30], 5 );
  } 
  translate([80,0,0]){
    semi_rounded_cube( [25,30,30], 5 );
  }
  translate([-80,0,0]){
    semi_rounded_cube( [25,30,30], 5 );
  }
  for( i = [-1,1] ){
    translate([40,25*i,0]){
      semi_rounded_cube( [60,10,30], 5 );
    }
    translate([-40,25*i,0]){
      semi_rounded_cube( [60,10,30], 5 );
    }
  }
}

module body_top_split(){
  tol = 1;
  translate( [-45, 0, 5] ){
    union(){
      difference(){
/*         cylinder(r=15+tol, h = 40, center=true);
         cylinder(r=15, h = 41, center=true);
         translate([-10,0,0]){
           cube( size =[20,40, 41], center=true);
         }*/
      }
      translate([15+tol/2, 0,0]){
        cube( size=[tol, 40, 40], center=true);
      }

      for( i =[-1,1] ){
        translate([(15+tol/2)/2,(40/2 -5/2)*i, 0]){
          rotate( i*atan( 5/15),[0,0,1]){
            cube( size=[16, tol, 40], center=true);
          }
        }

        translate([0, i*35, 0]){
          cube( size =[tol,2*(35-15), 40], center=true);
        }
        // screw_holes
        translate([0, i*30, 0]){
          translate([0,0,-10]){
            rotate(90,[0,1,0]){
              cylinder(r=3,h=12, center=true);
            }
          }
        }
      }
      translate([15,0,-10]){
        rotate(90,[0,1,0]){
          cylinder(r=3,h=14, center=true);
        }
      }
    }
  }
}

module body_top_connectors(){
  translate( [-45, 0, 0] ){
    union(){
      translate([15,0,0]){
        cube( size = [10+2,10, 20], center=true);
      }
      for( i =[-1,1] ){
        translate([0, i*30, 0]){
           cube( size =[10,10, 20], center=true);
        }
      }       
    } 
  }
}

module body_screw_holes(){
  for( i = [1, -1] ){
    for( j = [1, -1] ){
      translate( [45*i, 5*j, -48] ){
        cylinder(r=3, h = 22, center=true, $fn=10);
      }
    }
  }
}

module body_top(){
  union(){
    difference(){
      union(){
        translate( [0, 0, 5] ){
          translate([5,0,0]){
            difference(){
              cube(size = [ 230, 90, 10], center=true);
              for(i = [1,-1]){
                for( j = [1,-1]){
                  translate([115*i,45*j,0]){
                    rotate( 45 * (j-1) + 45 * (i-1) + 45*(i-1)*(j+1), [0,0,1]){ 
                      rounded_corner();
                    }
                  }
                } 
              }
            }
          }
          translate( [120 -10, 0, 0]){
            cylinder( r = 25, h= 10, center = true);
          } 
        }
        translate( [45, 0, -24] ){
          cylinder(r=15, h = 48, center=true);
        }    
        translate( [-45, 0, -24] ){
          cylinder(r=15, h = 48, center=true);
        }
        body_top_connectors();
      } 
      body_top_split(); 
      body_top_mesh_holes();  

      for( i = [1,-1] ){
        translate( [i*45, 0, -24 + 10 + 0.1 + 10] ){
          cylinder(r=10, h = 48, center=true);
          translate([0, 0, -24 ]){
            intersection(){
              cylinder(r=10, h = 20, center=true);
              rotate( 90, [1,0,0] ){
                cylinder(r=10, h = 40, center=true);
              }
            }
          }
        }
      }    
  
      // screw holes_for head
      translate( [120 -10, 0, 5]){
        for( i = [1,-1]){
          translate([0,i*(35-15/2),0]){
            cylinder( r = 3, h = 20, center=true);
          }
        }
        translate([-(35-15/2),0,0]){
          cylinder( r = 3, h = 20, center=true);
        }
      } 
      for( i = [-1, 1]){
        // holes for the pi
        translate( [45, i*47.5/2, 5]){
          cylinder( r = 2.5, h = 11, center=true, $fn = 10);
        }
        // holes for the battery
        for( j = [-1, 1]){
          translate( [-45 + j*15, i*55/2, 5]){
            cylinder( r = 3, h = 11, center=true, $fn = 10);
          }
        }
      }

      body_screw_holes();

      body_holes();

    }
    difference(){
      body_top_connectors();
      body_top_split();
    }
  }    
}

module body_bottom(){
  difference(){
    union(){
      translate( [0, 0,  -47 - 5 - 1] ){
        cube(size = [ 200, 70, 10], center=true);
      }
    } 
    body_screw_holes();
    body_holes();

    translate( [0, 0,  -47 - 5 - 1] ){
      body_base_mesh_holes();
    }
    translate( [0, 0,  -47 - 5 - 1 -5] ){
      for(i = [1,-1]){
        for( j = [1,-1]){
          translate([100*i,35*j,10]){
            rotate( 45 * (j-1) + 45 * (i-1) + 45*(i-1)*(j+1), [0,0,1]){ 
              scale([0.5,0.5, 1.5]){
                rounded_corner();
              }
            }
          }
        }
      }
    }    
  }
}

module body(){
  body_top();
  body_bottom();
}

body();
