use <thigh_joint.scad>
use <hip_joint.scad>
use <calf.scad>

function get_rotation(vec,t) = ( vec[min(floor(t*5),4)] * (1-(t*5-floor(t*5))) +  vec[min(floor(t*5)+1,4)] * (t*5-floor(t*5)));

module leg(rotation_vector, t){
  //echo( rotation_vector[0][floor(t*5)] * (t*5-floor(t*5)) );
  rotate(get_rotation(rotation_vector[0], t),[0,0,1]){
    hip_joint(true);
  
    translate([0,42+5 + 20,- 8 - 10 - 7.5]){
      rotate( 90, [0,1,0]){
        rotate( get_rotation(rotation_vector[1], t), [0,0,-1]){
          translate( [0,0, 53/2]){
            thigh_joint(true);
  
            rotate( -90, [0,1,0]){
              translate([-53/2, 80, 0]){
                rotate(  get_rotation(rotation_vector[2], t), [-1,0,0]){
                  calf(true);
                }
              }
            }
          }
        }
      }
    }
  }
}

leg();

