module split_joint(){
  union(){
    translate([0.25,0,0]){
      cube( size=[0.5,30,20], center = true );
    }
    translate([6.0,0,0]){
      cube( size=[6,12,6], center = true );
    }
    translate([-5,0,0]){
      rotate(90,[0,1,0]){
        translate([0,2.5,0]){
          cylinder( h = 20, r = 1.5, center = true, $fn = 10);
        }
        translate([0,-2.5,0]){
          cylinder( h = 20, r = 1.5, center = true, $fn = 10);
        }
      }
    }
  }
}
