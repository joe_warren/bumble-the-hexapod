module hk_servo_hex_top(tol=0){
  height = 2.5;
  union(){
    translate([0,0, -height/2]){
      for( i = [1:6] ){
        rotate( i*360/6, [0,0,1]){
          translate([15-2.5, 0, 0]){
            cylinder( h = height + tol, r = 2.5 + tol, $fn=20, center=true );
          }
          translate([15-2.5, 0, 0]){
            cylinder( h = height + tol, r = 2.5 + tol, $fn=20, center=true);
          }
          translate([15-2.5, 0, 0]){
            for( k = [5, -5]){
              rotate( k,[0,0,1]){ 
                translate([-7.5 + 2.5, 0, 0]){
                  cube( size=[15-5, 5+ 2* tol, height+ tol], center=true );
                }
              }
            }
          }
        } 
      }
    }
    translate( [0,0,-5/2] ){

      cylinder( h = 2, r1=4, r2 = 5 + tol, $fn=20, center=true );

      cylinder( h = 5 + tol, r = 4 + tol, $fn=20, center=true );
    }
  }
}

module hk_servo(rotation = 0){
  color("white"){
    rotate(rotation, [0,0,1]){
      hk_servo_hex_top();
    }
  }
  color("DimGray"){
    union(){
      translate( [0,0, -8 + 3]){
        cylinder( r = 3, h = 2, center=true );
      }
      translate( [0,0, -8 + 1]){
        cylinder( r1 = 15/2, r2 = 10/2, h = 2, center=true );
      }
      translate([0, 10, 0]){
        translate([0, 0, -8]){
          translate([0,0,- 36/2]){
            cube( size=[20, 40,36], center=true );
          }
          translate([0, 0, -10 + 2/2]){
            difference(){
              cube( size=[20, 56, 2], center=true );
              for(xt = [5, -5]){
                for(yt = [-20-4, 20+4]){
                  translate([xt, yt, 0]){
                    cylinder( r= 2, h = 3, center=true, $fn = 10);
                  }
                }
              }
            }
          }
          translate([0,-10 - 10 -2 ,-36+1]){
            cube( size=[8, 4, 2], center=true );
          }
        }
      }
    }
  }
}

hk_servo();

