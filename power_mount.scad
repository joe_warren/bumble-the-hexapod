module power_converter_holes(){
  for( i = [1,-1]){
    for( j = [1,-1]){
      translate([i*(24+3)/2, j*(55+3)/2, 0]){
        cylinder(r=2, h=20, center=true, $fn=20);
      }
    }
  }
}
module veroboard_slot(){
  difference(){
    cube([24,80,20], center=true);
    cube([12,80+0.1,20+0.1], center=true);
    cube([19,80+0.1,2.5], center=true);
    translate([0,(80-20)/2+0.1,20/2]){
      cube([19,20,28], center=true);
    }
  }
}
module battery_compartment_holes(){
  rotate([0,90,0]){
    for( i = [1,-1]){
      translate([i*20/2,0,0]){
        cylinder( r=2, h=20, center=true );
      }
    }
  }
}
module body_mount_holes(){
  for( i = [-1, 1]){
    translate( [0, i*55/2, 0]){
      cylinder( r = 3, h = 20, center=true, $fn = 10);
      translate( [0, 0, 10]){
        cylinder( r = 6, h = 12, center=true, $fn = 32);
      }
    }
  }
}

module power_switch_hole(){
  rotate([90,0,0]){
    cylinder( r=3.5, h=20, center=true );
  }
}

module power_mount(){
  union(){
    difference(){
      union(){
        cube([50+25+10,80+10, 25+10], center=true); 
        translate([12.5,0,-(25+10+10)/2]){
          cube([18,80+10, 10+0.1], center=true); 
        }
      }
      translate([0,5,5]){
        cube([50+25,80+10+0.1, 25+10+0.1], center=true); 
      }
      translate([12.5,0,-10]){
        power_converter_holes();
      }
      translate([12.5,-(80+10)/2,12]){
        power_switch_hole();
      }
      translate([40,0,3]){
        battery_compartment_holes();
      }

      translate([12.5,0,-(25+10+10)/2]){
        body_mount_holes();
      }
    }
    translate([-28.5,-1,-5]){
      veroboard_slot();
    }
  }
}

power_mount();
